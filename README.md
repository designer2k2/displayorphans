# Display Orphans Dokuwiki Plugin

[![MIT License](https://svgshare.com/i/TRb.svg)](https://opensource.org/licenses/MIT)
[![DokuWiki Plugin](https://svgshare.com/i/TSa.svg)](https://www.dokuwiki.org/dokuwiki)
[![Plugin Home](https://svgshare.com/i/TRw.svg)](https://www.dokuwiki.org/plugin:displayorphans)
[![Gitlab Repo](https://svgshare.com/i/TRR.svg)](https://gitlab.com/JayJeckel/displayorphans)
[![Gitlab Issues](https://svgshare.com/i/TSw.svg)](https://gitlab.com/JayJeckel/displayorphans/issues)
[![Gitlab Download](https://svgshare.com/i/TT5.svg)](https://gitlab.com/JayJeckel/displayorphans/-/archive/master/displayorphans-master.zip)

The Display Orphans Plugin can display tables of orphaned, wanted, and linked pages.

## Installation

Search and install the plugin using the [Extension Manager](https://www.dokuwiki.org/plugin:extension) or install directly using the latest [download url](https://gitlab.com/JayJeckel/displayorphans/-/archive/master/displayorphans-master.zip), otherwise refer to [Plugins](https://www.dokuwiki.org/plugins) on how to install plugins manually.

## Usage

| :warning: **ATTENTION** :warning: |
|-|
| **The following elements automatically disable cacheing of their containing page, similar to including the `~~NOCACHE~~` directive.** |

The plugin offers three block elements that expand into tables listing appropriate pages and other information.

| Element | Description |
|:-|:-|
| `<<display orphaned>>` | Displays a table of orphaned pages, ie pages that exist but aren't linked to. |
| `<<display wanted>>` | Displays a table of wanted pages, ie pages that do not exist but are linked to. |
| `<<display linked>>` | Displays a table of linked pages, ie pages that do exist and are linked to. |

## Configuration Settings

The plugin provides several settings that can be modified through the [Configuration Manager](https://www.dokuwiki.org/config:manager).

| Option | Default | Description |
|:-|:-|:-|
| `show_table_header` | yes | Specifies whether generated tables should include a header. |
| `sort_table_ascending` | yes | Specifies whether generated tables should sort in ascending order; otherwise sort in descending order. |
| `ignore_orphaned_pages` | empty | Space-separated list of pages to ignore when generating tables of orphaned pages. |
| `ignore_orphaned_namespaces` | empty | Space-separated list of namespaces to ignore when generating tables of orphaned pages. |
| `ignore_wanted_pages` | empty | Space-separated list of pages to ignore when generating tables of wanted pages. |
| `ignore_wanted_namespaces` | empty | Space-separated list of namespaces to ignore when generating tables of wanted pages. |
| `ignore_linked_pages` | empty | Space-separated list of pages to ignore when generating tables of linked pages. |
| `ignore_linked_namespaces` | empty | Space-separated list of namespaces to ignore when generating tables of linked pages. |

## Security

The plugin has no abnormal security concerns related to the provided functionality.
