<?php
/*
 * Display Orphans Plugin
 * Copyright (c) 2016 Jay Jeckel
 * Licensed under the MIT license: https://opensource.org/licenses/MIT
 * Permission is granted to use, copy, modify, and distribute the work.
 * Full license information available in the project LICENSE file.
*/

$meta['show_table_header'] = array('onoff');
$meta['sort_table_ascending'] = array('onoff');

$meta['ignore_orphaned_pages'] = array('string');
$meta['ignore_orphaned_namespaces'] = array('string');
$meta['ignore_wanted_pages'] = array('string');
$meta['ignore_wanted_namespaces'] = array('string');
$meta['ignore_linked_pages'] = array('string');
$meta['ignore_linked_namespaces'] = array('string');

?>